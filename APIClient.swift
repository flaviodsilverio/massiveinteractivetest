//
//  APIClient.swift
//  MassiveInteractiveTest
//
//  Created by Flávio Silvério on 11/03/17.
//  Copyright © 2017 Flávio Silvério. All rights reserved.
//

import Foundation
import CoreData

//in theory, this client could adapt and be part of any view model, that way we could partially update the app, however, we only have one endpoint, that being said we can "hardcode it here", still will create methods...


class APIClient {

    var apiManager = APIManager()
    var context : NSManagedObjectContext?
    
    static let sharedInstance = APIClient()
    
//    init(withContext moc: NSManagedObjectContext) {
//        context = moc
//    }
    
    func getMovies(){
    
    }
    
    func getShows(){
    
    }
    
    func getLists(){
    
    }
    
    func getAllData(completion:@escaping (_ success: Bool, _ error: String?) -> ()) {
    
        apiManager.perform(apiCallWithURLString: "https://static-rocket.surge.sh/home") {
            [weak self] (success, data) in
            
            if success {
            
                do {
                    let json = try JSONSerialization.jsonObject(with: data as! Data, options: .allowFragments)

                    DataManager.createModels(withData: json as! [String : AnyObject], inContext: (self?.context!)!, completion: { (success) in
                        
                        if success {
                            completion(true, "")
                        } else {
                            completion(false, "Error Saving the Data")
                        }
                    
                    })
                    
                } catch {
                    completion(false, "Error Parsing the JSON")
                }
                
                
            } else {
                completion(false, data as? String)
            }
        }
        
    }
    
    func get(imageWithURLString urlString: String,
             completion: @escaping (_ success: Bool,_ image: Data?)->()){
        
        apiManager.perform(apiCallWithURLString: urlString) {
             (success, data) in
            
            if success == true {
            
                completion(true, data as? Data)
                
            } else {
                
                completion(false, nil)

            }
        
        }
    }
    
    
}
