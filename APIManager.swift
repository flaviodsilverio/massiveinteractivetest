//
//  APIManager.swift
//  MassiveInteractiveTest
//
//  Created by Flávio Silvério on 11/03/17.
//  Copyright © 2017 Flávio Silvério. All rights reserved.
//

import Foundation

//This class only knows how to make network Requests, it has no idea on how to do anythin else. A client will have to use it.

class APIManager {

    let urlSession = URLSession.shared
    
    func perform(apiCallWithURLString urlString: String,
                 completion: @escaping(_ success: Bool, _ data: Any)->()){
    
        guard let url = URL(string: urlString) else {
            completion(false, "Invalid URL String")
            return
        }
        
        urlSession.dataTask(with: url) { (data, response, error) in

            if error != nil {
                completion(false, error!.localizedDescription)
            }
            
            guard let httpResponse = response as? HTTPURLResponse else {
                completion(false, "Invalid URL String")
                return
            }
            
            switch httpResponse.statusCode {
            
            case 200:
                completion(true, data!)
                break;
            
            default:
                completion(false, "Request Returned: \(httpResponse.statusCode)")
                break;
            }
            
        }.resume()
    }
    
}
