//
//  CollectionCellTableViewCell.swift
//  MassiveInteractiveTest
//
//  Created by Flávio Silvério on 11/03/17.
//  Copyright © 2017 Flávio Silvério. All rights reserved.
//

import UIKit

protocol CollectionTableViewCellDelegate: class {
    func did(selectItem item: Item)
}

class CollectionTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, CollectionTableViewCellDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    
    weak var delegate : CollectionTableViewCellDelegate?
    
    var list : Entry? {
        didSet{
            collectionView.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        let nib = UINib(nibName: "ItemCollectionCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "itemCell")
        
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    //MARK: Collection View Data Source
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        guard let currentList = list as? ListEntry else { return 1 }
        
        return (currentList.items?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "itemCell", for: indexPath) as! ItemCollectionCell
        
        let imageView = cell.imageView
        
        if list is ListEntry {
            
            let itemSet = (list as! ListEntry).items
            let itemArray = Array(itemSet!)
            let item = itemArray[indexPath.row]
            imageView?.set(imageForItem: item as! Item)
            
        } else if let listITem = list as? ItemEntry {
            imageView?.frame = cell.bounds
            imageView?.set(imageForItem: listITem.item!)
            
        }
        
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard (list as? ListEntry) != nil else { return }
        
        let itemSet = (list as! ListEntry).items
        let itemArray = Array(itemSet!)
        let item = itemArray[indexPath.row]

        self.delegate?.did(selectItem: item as! Item)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        guard (list as? ListEntry) != nil else {
            return CGSize(width: self.frame.size.width, height: self.frame.size.width / 7)
        }
        return CGSize(width: 100, height: 150)
    }

    //MARK: Delegation
    internal func did(selectItem item: Item) {
        self.delegate?.did(selectItem: item)
    }

}
