//
//  CoreData+Extensions.swift
//  MassiveInteractiveTest
//
//  Created by Flávio Silvério on 11/03/17.
//  Copyright © 2017 Flávio Silvério. All rights reserved.
//

import CoreData

extension NSManagedObject {
    
    class var entityName: String! {
        get {
            if #available(iOS 10.0, *) {
                return self.entity().managedObjectClassName.components(separatedBy: ["."]).last!
            } else {
               return self.entityName
            }
        }
    }
    
    public static func findOrCreate(objectWithPredicate predicate: NSPredicate, inManagedObjectContext moc: NSManagedObjectContext) -> NSManagedObject{
        
        //First we should fetch an existing object in the context as a performance optimization
        
        guard let existingObject = moc.find(objectMatchingPredicate: predicate) else {
            
            return fetchOrCreate(objectMatchingPredicate: predicate, inContext: moc)
            
        }
        
        return existingObject
        
    }
    
    public static func fetch(OrCreateWithID id: String, inContext moc: NSManagedObjectContext) -> NSManagedObject{
        
        let obj = fetchOrCreate(objectMatchingPredicate: NSPredicate(format: "id == %@", id), inContext: moc)
        return obj
    }
    
    public static func fetchOrCreate(objectMatchingPredicate predicate: NSPredicate, inContext moc: NSManagedObjectContext) -> NSManagedObject{
        
        //if it's not in memory, we should execute a fetch to see if it exists
        let fetch = NSFetchRequest<NSManagedObject>(entityName: self.entityName)
        
        fetch.predicate = predicate
        fetch.fetchLimit = 1
        fetch.returnsObjectsAsFaults = false
        
        do {
            
            let objects = try moc.fetch(fetch) 
            
            if objects.count > 0 {
                return objects.first!
            }
            
        } catch let error {
            print(error)
        }
        
        //If it didn't exist in memory and wasn't fetched, we should create a new object
        return createObject(inContext: moc)
        
    }
    
    public static func createObject(inContext moc: NSManagedObjectContext) -> NSManagedObject{
        let newObject = NSEntityDescription.insertNewObject(forEntityName: entityName , into: moc)
        return newObject
    }
    
    public static func fetchAll(inContext moc:NSManagedObjectContext) -> [NSManagedObject]{
        
        let fetch = NSFetchRequest<NSManagedObject>(entityName: self.entityName)
        
        do {
            
            let objects = try moc.fetch(fetch)
            
            return objects
            
        } catch let error {
            print(error)
        }
        
        return []
    }
    
}

extension NSManagedObjectContext {
    
    public func find(objectMatchingPredicate predicate: NSPredicate) -> NSManagedObject? {
        
        for object in self.registeredObjects where !object.isFault {
            if predicate.evaluate(with: object) {
                return object
            }
        }
        
        return nil
    }
    
}
