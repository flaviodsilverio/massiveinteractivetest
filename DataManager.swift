//
//  DataManager.swift
//  MassiveInteractiveTest
//
//  Created by Flávio Silvério on 11/03/17.
//  Copyright © 2017 Flávio Silvério. All rights reserved.
//

import Foundation
import CoreData


class DataManager {
    
    class func createModels(withData data: [String: AnyObject],
                      inContext moc: NSManagedObjectContext,
                      completion:@escaping (_ success: Bool)->()){
        
        guard let entries = data["entries"] as? [[String:AnyObject]] else { completion(false); return }
        
        for entry in entries {
            if let type = entry["type"] as? String {
                
                if type == "ListEntry" {
                    
                    let listEntry = ListEntry.create(listEntryWithData: entry, inContext: moc)
                    
                    if let items = (entry["list"] as? [String:AnyObject])?["items"] as? [[String:AnyObject]] {
                        for item in items {
                            
                            let type = item["type"] as! String
                            
                            if type == "movie" {
                                
                                listEntry.addToItems(Movie.create(movieWithData: item, inContext: moc))
                                
                            } else if type == "show" {
                                
                                listEntry.addToItems(Show.create(showWithData: item, inContext: moc))

                            }
                        }
                        
                    }
                } else {
                
                    let itemEntry = ItemEntry.create(itemEntryWithData: entry, inContext: moc)
                    
                    if let item = entry["item"] as? [String:AnyObject] {
                        itemEntry.item = Link.create(linkWithData: item, inContext: moc)
                    }
                    
                    
                }
            }
            
        }
        
        do {
            try moc.save()
            completion(true)
        } catch {
            completion(false)
        }
        
    }
}
