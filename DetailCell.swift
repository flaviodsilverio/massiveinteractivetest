//
//  DetailCell.swift
//  MassiveInteractiveTest
//
//  Created by Flávio Silvério on 12/03/17.
//  Copyright © 2017 Flávio Silvério. All rights reserved.
//

import UIKit

class DetailCell: UITableViewCell {

    @IBOutlet weak var detailTitleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    
    //These are so so so nice:
    var detailText : String? {
        didSet{
            detailLabel.text = detailText
        }
    }
    
    var detailTitleText : String? {
        didSet{
            detailTitleLabel.text = detailTitleText
        }
    }
    
}
