//
//  Entry+CoreDataClass.swift
//  MassiveInteractiveTest
//
//  Created by Flávio Silvério on 11/03/17.
//  Copyright © 2017 Flávio Silvério. All rights reserved.
//

import Foundation
import CoreData


public class Entry: NSManagedObject {

    func fill(withData data: [String: AnyObject]) {
    
        id = data["id"] as? String
        type = data["type"] as? String
        title = data["title"] as? String
        template = data["template"] as? String
        
    }
    
    //this is very easy to do with a predicate, however since there's more than 1 type of list, I just went with a C style cycle
    class func fetchEntriesWithItems(inConext context: NSManagedObjectContext) -> [Entry] {
        
        let entries = Entry.fetchAll(inContext: context) as! [Entry]
        var auxLists = [Entry]()
        
        for l in entries {
            if l is ListEntry {
                if (l as! ListEntry).items!.count > 0 {
                    auxLists.append(l)
                }
            } else {
                auxLists.append(l)
            }
        }

        return auxLists
    }

}
