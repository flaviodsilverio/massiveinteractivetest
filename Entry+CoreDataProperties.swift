//
//  Entry+CoreDataProperties.swift
//  MassiveInteractiveTest
//
//  Created by Flávio Silvério on 11/03/17.
//  Copyright © 2017 Flávio Silvério. All rights reserved.
//

import Foundation
import CoreData


extension Entry {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Entry> {
        return NSFetchRequest<Entry>(entityName: "Entry");
    }

    @NSManaged public var id: String?
    @NSManaged public var type: String?
    @NSManaged public var title: String?
    @NSManaged public var template: String?

}
