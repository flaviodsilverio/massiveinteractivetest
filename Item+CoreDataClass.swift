//
//  Item+CoreDataClass.swift
//  MassiveInteractiveTest
//
//  Created by Flávio Silvério on 11/03/17.
//  Copyright © 2017 Flávio Silvério. All rights reserved.
//

import Foundation
import CoreData


public class Item: NSManagedObject {

    func fill(withData data: [String:AnyObject]) {

        badge = data["badge"] as? String
        genres = data["genres"] as? NSObject
        shortDesc = data["shortDescription"] as? String
        type = data["type"] as? String
        path = data["path"] as? String
        categories = data["categories"] as? NSObject
        scopes = data["scopes"] as? NSObject
        title = data["title"] as? String
        images = data["images"] as? NSObject
        customId = data["customId"] as? String
        offers = data["offers"] as? NSObject
        tagline = data["tagline"] as? String
        id = data["id"] as? String
        //averageUserRating = (data["averageUserRating"] as? Int16)!
        
    }
    
}
