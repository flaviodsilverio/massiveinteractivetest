//
//  Item+CoreDataProperties.swift
//  MassiveInteractiveTest
//
//  Created by Flávio Silvério on 12/03/17.
//  Copyright © 2017 Flávio Silvério. All rights reserved.
//

import Foundation
import CoreData


extension Item {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Item> {
        return NSFetchRequest<Item>(entityName: "Item");
    }

    @NSManaged public var averageUserRating: Int16
    @NSManaged public var badge: String?
    @NSManaged public var categories: NSObject?
    @NSManaged public var customId: String?
    @NSManaged public var genres: NSObject?
    @NSManaged public var id: String?
    @NSManaged public var image: NSData?
    @NSManaged public var images: NSObject?
    @NSManaged public var offers: NSObject?
    @NSManaged public var path: String?
    @NSManaged public var scopes: NSObject?
    @NSManaged public var shortDesc: String?
    @NSManaged public var tagline: String?
    @NSManaged public var title: String?
    @NSManaged public var type: String?
    @NSManaged public var mainImage: String?
    @NSManaged public var lists: NSSet?

}

// MARK: Generated accessors for lists
extension Item {

    @objc(addListsObject:)
    @NSManaged public func addToLists(_ value: ListEntry)

    @objc(removeListsObject:)
    @NSManaged public func removeFromLists(_ value: ListEntry)

    @objc(addLists:)
    @NSManaged public func addToLists(_ values: NSSet)

    @objc(removeLists:)
    @NSManaged public func removeFromLists(_ values: NSSet)

}
