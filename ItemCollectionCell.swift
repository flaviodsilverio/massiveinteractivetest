//
//  ItemCollectionCell.swift
//  MassiveInteractiveTest
//
//  Created by Flávio Silvério on 12/03/17.
//  Copyright © 2017 Flávio Silvério. All rights reserved.
//

import UIKit

class ItemCollectionCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    
    var item: Item? {
        didSet{
            self.imageView.set(imageForItem: item!)
        }
    }
}
