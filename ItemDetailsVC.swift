//
//  ItemDetailsVC.swift
//  MassiveInteractiveTest
//
//  Created by Flávio Silvério on 12/03/17.
//  Copyright © 2017 Flávio Silvério. All rights reserved.
//

import UIKit

class ItemDetailsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var tableData = [[String:String]]()
    
    var item : Item! {
        didSet {
            tableData = get(detailsForItem: item)
            self.title = item.title
        }
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 60

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! DetailCell
        
        cell.detailTitleText = tableData[indexPath.row]["fieldName"]
        cell.detailText = tableData[indexPath.row]["value"]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 300
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x:0, y:0, width: self.view.frame.width, height: 300))
        let imageView = UIImageView(frame: CGRect(x:0, y:0, width: 200, height: 300))
        
        view.addSubview(imageView)
        imageView.center = view.center
        imageView.set(imageForItem: item)
        
        return view
    }
}

// Just SO the code above isn't so "heavy"
extension ItemDetailsVC {

    func get(detailsForItem item: Item) -> [[String:String]]{
    
        var details = [[String:String]]()
        
        details.append(get(detail: item.title!, forKey: "Title"))
        details.append(get(detail: item.shortDesc!, forKey: "Description"))
        
        if item is Movie {
            details.append(get(detail: String((item as! Movie).releaseYear), forKey: "Release Year"))
        } else if item is Show {
            details.append(get(detail: String(describing: (item as! Show).availableSeasonCount), forKey: "Available Seasons"))
        }
        
        return details
    }
    
    func get(detail: String, forKey key: String) -> [String:String]{
        return ["fieldName":key, "value": detail]
    }
    
}
