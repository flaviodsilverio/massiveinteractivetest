//
//  ItemEntry+CoreDataClass.swift
//  MassiveInteractiveTest
//
//  Created by Flávio Silvério on 11/03/17.
//  Copyright © 2017 Flávio Silvério. All rights reserved.
//

import Foundation
import CoreData


public class ItemEntry: Entry {

    class func create(itemEntryWithData data: [String:AnyObject],
                      inContext moc: NSManagedObjectContext) -> ItemEntry{
        
        let itemEntry = ItemEntry.fetch(OrCreateWithID: data["id"] as! String, inContext: moc) as! ItemEntry
        itemEntry.fill(withData: data)
        
        return itemEntry
    }
    
}
