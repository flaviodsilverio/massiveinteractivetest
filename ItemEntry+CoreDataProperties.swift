//
//  ItemEntry+CoreDataProperties.swift
//  MassiveInteractiveTest
//
//  Created by Flávio Silvério on 11/03/17.
//  Copyright © 2017 Flávio Silvério. All rights reserved.
//

import Foundation
import CoreData


extension ItemEntry {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ItemEntry> {
        return NSFetchRequest<ItemEntry>(entityName: "ItemEntry");
    }

    @NSManaged public var item: Link?

}
