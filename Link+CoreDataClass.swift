//
//  Link+CoreDataClass.swift
//  MassiveInteractiveTest
//
//  Created by Flávio Silvério on 11/03/17.
//  Copyright © 2017 Flávio Silvério. All rights reserved.
//

import Foundation
import CoreData


public class Link: Item {

    class func create(linkWithData data: [String:AnyObject],
                           inContext moc: NSManagedObjectContext) -> Link{
        
        let link = Link.fetch(OrCreateWithID: data["id"] as! String, inContext: moc) as! Link
        link.fill(withData: data)
        
        link.mainImage = (data["images"] as? [String:String])?["hero7x1"]
        
        return link
    }
}
