//
//  Link+CoreDataProperties.swift
//  MassiveInteractiveTest
//
//  Created by Flávio Silvério on 11/03/17.
//  Copyright © 2017 Flávio Silvério. All rights reserved.
//

import Foundation
import CoreData


extension Link {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Link> {
        return NSFetchRequest<Link>(entityName: "Link");
    }

    @NSManaged public var entry: NSSet?

}

// MARK: Generated accessors for entry
extension Link {

    @objc(addEntryObject:)
    @NSManaged public func addToEntry(_ value: ItemEntry)

    @objc(removeEntryObject:)
    @NSManaged public func removeFromEntry(_ value: ItemEntry)

    @objc(addEntry:)
    @NSManaged public func addToEntry(_ values: NSSet)

    @objc(removeEntry:)
    @NSManaged public func removeFromEntry(_ values: NSSet)

}
