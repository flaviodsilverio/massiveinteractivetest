//
//  ListEntry+CoreDataClass.swift
//  MassiveInteractiveTest
//
//  Created by Flávio Silvério on 11/03/17.
//  Copyright © 2017 Flávio Silvério. All rights reserved.
//

import Foundation
import CoreData


public class ListEntry: Entry {

    class func create(listEntryWithData data: [String:AnyObject],
                      inContext moc: NSManagedObjectContext) -> ListEntry{
        
        let listEntry = ListEntry.fetch(OrCreateWithID: data["id"] as! String, inContext: moc) as! ListEntry
        listEntry.fill(withData: data)
        
        return listEntry
    }
    
}
