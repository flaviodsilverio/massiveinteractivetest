//
//  ListEntry+CoreDataProperties.swift
//  MassiveInteractiveTest
//
//  Created by Flávio Silvério on 11/03/17.
//  Copyright © 2017 Flávio Silvério. All rights reserved.
//

import Foundation
import CoreData


extension ListEntry {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ListEntry> {
        return NSFetchRequest<ListEntry>(entityName: "ListEntry");
    }

    @NSManaged public var items: NSSet?

}

// MARK: Generated accessors for items
extension ListEntry {

    @objc(addItemsObject:)
    @NSManaged public func addToItems(_ value: Item)

    @objc(removeItemsObject:)
    @NSManaged public func removeFromItems(_ value: Item)

    @objc(addItems:)
    @NSManaged public func addToItems(_ values: NSSet)

    @objc(removeItems:)
    @NSManaged public func removeFromItems(_ values: NSSet)

}
