//
//  ViewController.swift
//  MassiveInteractiveTest
//
//  Created by Flávio Silvério on 11/03/17.
//  Copyright © 2017 Flávio Silvério. All rights reserved.
//

import UIKit
import CoreData

class HomeVC: UIViewController, UITableViewDataSource, UITableViewDelegate, CollectionTableViewCellDelegate {

    var apiClient = APIClient.sharedInstance
    var managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var lists = [Entry]()
    
    private let refreshControl = UIRefreshControl()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        apiClient.context = managedObjectContext
        getData()
        
        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(getData), for: .valueChanged)
        
    }

    func getData(){
        
        apiClient.getAllData(completion: {
            [unowned self] (success, error) in
            
            if success == false, error != nil {
            
                let alert = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alert.addAction(okAction)
                
                DispatchQueue.main.sync {
                    self.tableView.refreshControl?.endRefreshing()
                    self.present(alert, animated: true, completion: nil)
                }

            } else {
 
                self.lists = Entry.fetchEntriesWithItems(inConext: self.managedObjectContext)
                
                DispatchQueue.main.sync {
                    self.tableView.refreshControl?.endRefreshing()
                    self.tableView.reloadData()
                }
            }
            
        })
    }

    //MARK: Table View Data Source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return lists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? CollectionTableViewCell

        cell?.list = lists[indexPath.section]
        cell?.delegate = self
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if lists[indexPath.section] is ItemEntry {
            return self.view.frame.width / 7 + 10
        } else {
            return 160
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return lists[section].title
    }
    
    func did(selectItem item: Item) {
        self.performSegue(withIdentifier: "showItemDetails", sender: item)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! ItemDetailsVC
        destination.item = sender as! Item
    }
    
  }

