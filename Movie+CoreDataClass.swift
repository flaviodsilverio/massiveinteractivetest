//
//  Movie+CoreDataClass.swift
//  MassiveInteractiveTest
//
//  Created by Flávio Silvério on 11/03/17.
//  Copyright © 2017 Flávio Silvério. All rights reserved.
//

import Foundation
import CoreData


public class Movie: Item {

    class func create(movieWithData data: [String:AnyObject],
                      inContext moc: NSManagedObjectContext) -> Movie{
    
        let movie = Movie.fetch(OrCreateWithID: data["id"] as! String, inContext: moc) as! Movie
        movie.fill(withData: data)
        
        movie.classification = data["classification"] as? NSObject
        movie.releaseYear = data["releaseYear"] as! Int16
        movie.watchPath = data["watchPath"] as? String
        movie.duration = data["duration"] as! Int16
        movie.mainImage = (data["images"] as? [String:String])?["poster"]
        
        return movie
    }
    
}
