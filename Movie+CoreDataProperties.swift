//
//  Movie+CoreDataProperties.swift
//  MassiveInteractiveTest
//
//  Created by Flávio Silvério on 11/03/17.
//  Copyright © 2017 Flávio Silvério. All rights reserved.
//

import Foundation
import CoreData


extension Movie {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Movie> {
        return NSFetchRequest<Movie>(entityName: "Movie");
    }

    @NSManaged public var classification: NSObject?
    @NSManaged public var releaseYear: Int16
    @NSManaged public var watchPath: String?
    @NSManaged public var duration: Int16

}
