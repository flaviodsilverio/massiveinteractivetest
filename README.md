Hello, here are some considerations/technical options, that may be helpful:

Architecture:
- Originally planned to go the MVVM route, however ended up going with almost Vanilla Apple MVC.

Compatibility:
- The Project is compatible with iOS 10+, that is because I've used the newest iOS 10 CoreData stack and haven't changed it to be compatible with iOS 9 or earlier. I can do so if you so desire.

UI:
- I focused mainly on functionality so the UI didn't have a lot of love. If this was a main requirement, I can easily give it a tidy-up.

Tests:
- Unfortunately I haven't written any tests. However, most of the errors (if not all) should be handled and you shouldn't experience any crashes, at least I haven't.

Persistance:
- You'll notice the App is built heavily upon CoreData. I wasn't sure of how you do things there, if you use persistence or not so, I went the CoreData way. However, you'll also notice that even if you launch the app, close it, turn off wifi, and relaunch it, nothing will be there and you'll see an empty Home Page (not movies and shows) and no data until you pull to refresh. That is, like I said, because I didn't know how you did things or wanted it. So home will only update it's UI after you successfully receive data and the other two will access persisted data. This is on purpose and I can change it if needed.

I've also implemented a nice (in my opinion) 3-step-fetch system.This is great performance wise. As you probably have understood, I do like CoreData.

Images ad Details Page: 
- For images, I've only used one for each item, both for the list and Details. I could've used more (as there were many) but since the test only mentioned a basic details page, for simplicity, I've only used 1. The Details page doesn't have a lot of details, since, as it stated, it was to be only a basic detail page. The details are still in memory so, if there's any need, that page can easily be expanded. 

Launching the App:
- Even if you don't have wifi or the connection times out, there's pull to refresh and you can pull (Home only) anytime.

I developed the app quite quickly and did everything by myself, even the asynchronous image loading in the UIImageView extension. 

If there's any request or doubt, please don't hesitate to contact me on flaviodsilverio@gmail.com, I'll be more than happy to hear from you.




PS: The UIImageView Extension, as you'll see, as a possible problem long term, which comes from the fact that I'm not checking on the asynchronous thread if it's the correct imageView/Item, this can be easily corrected by passing it an identifier and checking it. 

PS2: "Bill You Murray Me" doesn't do anything when touched, that's because on the test data there was no Bill Murray stuff like in the Example Website.