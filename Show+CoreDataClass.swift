//
//  Show+CoreDataClass.swift
//  MassiveInteractiveTest
//
//  Created by Flávio Silvério on 11/03/17.
//  Copyright © 2017 Flávio Silvério. All rights reserved.
//

import Foundation
import CoreData


public class Show: Item {

    class func create(showWithData data: [String:AnyObject],
                      inContext moc: NSManagedObjectContext) -> Show{
        
        let show = Show.fetch(OrCreateWithID: data["id"] as! String, inContext: moc) as! Show
        show.fill(withData: data)
        
        show.availableSeasonCount = Int16((data["availableSeasonCount"] as? Int)!)
        show.mainImage = (data["images"] as? [String:String])?["poster"]
        
        return show
    }
    
}
