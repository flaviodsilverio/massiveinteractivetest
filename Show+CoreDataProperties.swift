//
//  Show+CoreDataProperties.swift
//  MassiveInteractiveTest
//
//  Created by Flávio Silvério on 12/03/17.
//  Copyright © 2017 Flávio Silvério. All rights reserved.
//

import Foundation
import CoreData


extension Show {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Show> {
        return NSFetchRequest<Show>(entityName: "Show");
    }

    @NSManaged public var availableSeasonCount: Int16

}
