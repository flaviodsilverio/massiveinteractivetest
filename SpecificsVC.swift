//
//  SpecificsVC.swift
//  MassiveInteractiveTest
//
//  Created by Flávio Silvério on 11/03/17.
//  Copyright © 2017 Flávio Silvério. All rights reserved.
//

import UIKit

class SpecificsVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    //This VC would trigger a fetch for it's most recent data, however, since there's no way to get specific data, I'll just consider that Home did get all the data and will only fetch from the database
    var items : [Item]?
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if self.tabBarController?.selectedIndex == 1 {
            items = Movie.fetchAll(inContext: context) as? [Item]
            self.title = "Movies"
        } else {
            items = Show.fetchAll(inContext: context) as? [Item]
            self.title = "Shows"
        }
        
        let nib = UINib(nibName: "ItemCollectionCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "itemCell")
        
        
    }
    

    // MARK: - Collection View Methods
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "itemCell", for: indexPath) as! ItemCollectionCell
        
        cell.item = items?[indexPath.row]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "showItemDetails", sender: items?[indexPath.row])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! ItemDetailsVC
        destination.item = sender as! Item
    }

}
