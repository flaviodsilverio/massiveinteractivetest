//
//  UIImageView+Extensions.swift
//  MassiveInteractiveTest
//
//  Created by Flávio Silvério on 12/03/17.
//  Copyright © 2017 Flávio Silvério. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
   
    func set(imageForItem item: Item) {
        
        if item.image == nil {
            
            self.image = UIImage(named: "placeholder")
            
            APIClient.sharedInstance.get(imageWithURLString: item.mainImage!,
                                         completion: { [weak self] (success, data) in
                                            
                 if success {
                    item.image = data as NSData?
                    DispatchQueue.main.sync {
                        self?.image = UIImage(data: data!)
                    }
                 }
                                            
            })
            
        } else {
            self.image = UIImage(data: item.image as! Data)
        }
        
    }

}
